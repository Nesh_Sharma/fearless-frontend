import React, {useEffect, useState} from 'react';
function ConferenceForm() {
    const[locations, SetLocations] = useState([]);
    const[name, SetNames] = useState("");
    const[start, SetStart] = useState("");
    const[end, SetEnd] = useState("");
    const[description, SetDescription] = useState("");
    const[maxPresentations, SetMaxPresentations] = useState("");
    const[maxAttendees, SetMaxAttendees] = useState("");
    const[location, setLocation] = useState("");


    const handleNameChange = (event) => {
        const value = event.target.value
        SetNames(value)
    }
    const handleStartChange = (event) => {
        const value = event.target.value
        SetStart(value);
    }
    const handleEndChange = (event) => {
        const value = event.target.value
        SetEnd(value);
    }
    const handleDescriptionChange = (event) => {
        const value = event.target.value
        SetDescription(value);
    }
    const handleMaxPresentationsChange = (event) => {
        const value = event.target.value
        SetMaxPresentations(value);
    }
    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value
        SetMaxAttendees(value);
    }
    const handleLocationChange = (event) =>{
        const value = event.target.value
        setLocation(value);
    }


    const fetchData = async ()=> {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            SetLocations(data.locations)
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.starts = start;
        data.ends = end;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;

        const conferenceUrl = 'http://localhost:8000/api/conferences/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'applicaiton/json',
            },
        }

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok){
            const newConference = await response.json();
            console.log(newConference);

            SetNames('');
            SetStart('');
            SetEnd('');
            SetDescription('');
            SetMaxPresentations('');
            SetMaxAttendees('');
            setLocation('');
        }
    }
    useEffect(() => {
        fetchData();
      }, []);
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Conference!</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartChange} value={start} placeholder="Starts" required type="date" name = "starts" id="starts" className="form-control"/>
                <label htmlFor="room_count">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input  onChange={handleEndChange} value={end} placeholder="Ends" required type="date" name = "ends" id="ends" className="form-control"/>
                <label htmlFor="room_count">Ends</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleDescriptionChange} value={description} placeholder="Description" required type="text" name = "description" id="description" className="form-control"/>
                <label htmlFor="room_count">Description</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxPresentationsChange} value={maxPresentations} placeholder="Max_presentations" required type="text" name = "max_presentations" id="max_presentations" className="form-control"/>
                <label htmlFor="room_count">Max presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxAttendeesChange} value={maxAttendees} placeholder="Max_attendees" required type="text" name = "max_attendees" id="max_attendees" className="form-control"/>
                <label htmlFor="room_count">Max attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} value={location} required id="location"  name = "location" className="form-select">
                  <option>Choose a location</option>
                    {locations.map(location => {
                    return (
                        <option key={location.href} value={location.id}>
                            {location.name}
                        </option>
                    )
                })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default ConferenceForm;