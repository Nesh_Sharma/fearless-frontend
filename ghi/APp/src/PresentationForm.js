import React, {useEffect, useState} from 'react';

function PresentationForm(){
    const[presentations, setPresentations] =  useState([]);
    const [name, setName] = useState("")
    const [email, setEmail] = useState("");
    const [companyName, setCompanyName] = useState("");
    const [title, setTitle] = useState("");
    const [synopsis, setSynopsis] = useState("");
    const[presentation, setPresentation] =  useState([]);

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleChangeEmail = (event) => {
        const value = event.target.value;
        setEmail(value);
      }
    const handleCompanyName = (event) => {
        const value = event.target.value;
        setCompanyName(value);
    }
    const handleTitleChange = (event) => {
        const value = event.target.value;
        setTitle(value);
    }
    const handleSynopsisChange = (event) => {
        const value = event.target.value;
        setSynopsis(value);
    }
    const handlePresentationChange = (event) => {
        const value = event.target.value;
        setPresentation(value);
    }        
    
    const fetchData = async ()=> {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            setPresentations(data.conferences)
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.presenter_name = name;
        data.conference = presentation;
        data.company_name = companyName;
        data.title = title;
        data.presenter_email = email;
        data.synopsis = synopsis;

        const presentationUrl = `http://localhost:8000${presentation}presentations/`
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'applicaiton/json',
            },
        }

        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok){
            const newPresentation = await response.json();
            console.log(newPresentation);

            setName('');
            setEmail('');
            setCompanyName('');
            setSynopsis('');
            setTitle('');
            setPresentation('');
        }
    }
    useEffect(() => {
        fetchData();
      }, []);
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleChangeEmail} value={email}placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCompanyName} value={companyName} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"/>
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleTitleChange} value={title} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea onChange={handleSynopsisChange} value={synopsis} className="form-control" id="synopsis" rows="3" name="synopsis"></textarea>
              </div>
              <div className="mb-3">
                <select onChange={handlePresentationChange} value={presentation} required name="conference" id="conference" className="form-select">
                  <option >Choose a conference</option>
                  {presentations.map(conferences => {
                    return (
                    <option key={conferences.href} value={conferences.href}>
                        {conferences.name}
                    </option>
                    );
                })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}
export default PresentationForm;